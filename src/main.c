#include <stdlib.h>
#include <stdio.h>

int main(void)
{
    printf("Hello, world\n");
    char *p = malloc(2 * sizeof( char ));
    *p = 'c';
    printf("*p : %c\n", *p);
    free(p);
    return 0;
}
